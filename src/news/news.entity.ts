import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "news"})
export class News {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    title: string;
    @Column()
    url_image: string;
    @Column({type: "timestamp" ,default : () => "CURRENT_TIMESTAMP"})
    publication_date: Date;
    @Column()
    place: string;
    @Column({nullable: true})
    author: string;   
    @Column()
    content: string;
}