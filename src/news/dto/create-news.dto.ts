export class CreateNewsDto {
    title: string;
    url_image: string;
    publication_date: Date;
    place: string;
    author: string;
    content: string;
}