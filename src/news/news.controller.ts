import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import { CreateNewsDto } from './dto/create-news.dto';
import { NewsService } from './news.service';
import { News } from './news.entity';

@Controller('news')
export class NewsController {
    constructor(private newsService: NewsService) {}
    
    @Post()
    createNews(@Body() newNews: CreateNewsDto): Promise<News> {
        return this.newsService.createNews(newNews);
    }

    @Get()
    async newsList(): Promise<News[]> {
        const response = await this.newsService.newsList();
        return this.newsService.newsList();
    }

    @Get(':id')
    getNews(@Param('id', ParseIntPipe) id: number) {
        return this.newsService.getNews(id);
    }

    @Delete (':id')
    deleteNews(@Param('id', ParseIntPipe) id: number) {
        return this.newsService.deleteNews(id);
    }

    @Put(':id')
    updateNews(@Param('id') id : number, @Body() updateNews: CreateNewsDto) {
        return this.newsService.udpateNews(id, updateNews);
    }
}
