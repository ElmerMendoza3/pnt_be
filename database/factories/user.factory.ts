import { News } from 'src/news/news.entity';
import { setSeederFactory } from 'typeorm-extension';


export default setSeederFactory(News, async (faker) => {
  const news = new News();

  news.title = faker.lorem.sentence();
  news.place = faker.address.city();
  news.author = faker.name.fullName();
  news.content = faker.lorem.words();
  news.url_image = faker.image.imageUrl();
  return news;
});
