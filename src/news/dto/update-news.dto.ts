export class UpdateNewsDto {
    title?: string;
    publication_date?: Date;
    place?: string;
    author?: string;
    content?: string;
}