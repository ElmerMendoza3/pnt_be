```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Migrations
```bash
# Ejecutar Migraciones
$ npm run mig:run

# Ejecutar Sedders
# npm run seeders
```

## Configuración
```bash
# Modificar el archivo app.module.ts y .env
# con las credenciales de autenticación de la base de datos
# como username, password, database 
    

