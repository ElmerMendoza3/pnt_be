import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateNewsDto } from './dto/create-news.dto';
import { News } from './news.entity';
import { Repository } from 'typeorm';
import { UpdateNewsDto } from './dto/update-news.dto';

@Injectable()
export class NewsService {

    constructor(@InjectRepository(News) private newsRepository: Repository<News>) {}
    createNews(news: CreateNewsDto) {
      const newNews = this.newsRepository.create(news);
      return this.newsRepository.save(newNews);
    }
    async udpateNews(id: number, updatedNews: UpdateNewsDto) {
      const news = await this.newsRepository.findOne({
        where: { 
          id 
        },
      });

      if(!news){
        return new HttpException('News not found', HttpStatus.NOT_FOUND)
      }
      return this.newsRepository.save( {...news, ...updatedNews});
    }

    newsList(){
      return this.newsRepository.find({
        order: {
          id: 'DESC',
        },
      });
    }

    async deleteNews(id: number){
 
      const result =  await this.newsRepository.delete({id});
      if(result.affected === 0){
        return new HttpException('News not found', HttpStatus.NOT_FOUND)
      }
      return result;
    }

    async getNews(id: number){  
      const news = await this.newsRepository.findOne({
        where: { 
          id 
        },
      });

      if(!news){
        return new HttpException('News not found', HttpStatus.NOT_FOUND)
      }
      return news
    }

}
