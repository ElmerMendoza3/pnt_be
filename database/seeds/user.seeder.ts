import { News } from 'src/news/news.entity';
import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';

export default class UserSeeder implements Seeder {
  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager,
  ): Promise<any> {

    const newsFactory = await factoryManager.get(News);
    await newsFactory.save();
    await newsFactory.saveMany(40);

    console.log('Seeding completed for News entity.');
  }
}
