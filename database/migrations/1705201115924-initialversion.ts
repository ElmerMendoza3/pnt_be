import { MigrationInterface, QueryRunner } from "typeorm";

export class Initialversion1705201115924 implements MigrationInterface {
    name = 'Initialversion1705201115924'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "news" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "url_image" character varying NOT NULL, "publication_date" TIMESTAMP NOT NULL DEFAULT now(), "place" character varying NOT NULL, "author" character varying, "content" character varying NOT NULL, CONSTRAINT "PK_39a43dfcb6007180f04aff2357e" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "news"`);
    }

}
